import { isNull } from "util";
import Vue from 'vue';

export function Chart(canvasId) {

    this.properties = {
        id: canvasId,
        channels: [],
        colors: [], //custom colors for channels
        isRightPanelOpen: true,
        'colors': {
            bg: 'rgba(60,60,60,1)',
            axes: 'rgba(240,240,240,1)',
            horiz: 'rgba(130,130,130,1)',
            label: 'rgba(240,240,240,1)',
        },
        'fnStringToValue': function (string) { return parseInt(string); },
        'fnValueToString': function (value) { return value.toString(); },

        'minVal': undefined,
        'maxVal': undefined,
        'data': [],
        isDataUpdated: true,
        'dataLength': 20,
        'showValue': null,

        'label': null,
        'axesY': [],
        'axesX': [],
        'axisH': 20,
        'deltaVisual': 0,
        'valOffset': 0,

        'height': 0,
        'width': 0,
        'x': 0,
        'y': 0,
        'offsetX': 48,
        'offsetY': 22,
        geometry: {
            height: 200,
            width: 400,
        },
        position: {
            x: 0,
            y: 0
        },
        canvas: '',
        context: '',
        boundingBox: ''
    };

    this.setup = function () {
        var props = this.properties;
        var id = props.id;

        props.canvas = document.getElementById(id);
        if (props.canvas.getContext) {
            props.context = props.canvas.getContext("2d", { alpha: true });

            /*             var clientW = props.canvas.parentNode.clientWidth;
                        var clientH = props.canvas.parentNode.clientHeight; */

            /*             props.context.canvas.setAttribute('width', clientW);
                        props.context.canvas.setAttribute('height', clientH);
                        props.geometry.height = props.canvas.clientHeight;
                        props.geometry.width = props.canvas.clientWidth; */
        }
    }

    this.redraw = function (params) {
        var properties = this.properties;
        var clientW = properties.canvas.parentNode.clientWidth;
        var clientH = properties.canvas.parentNode.clientHeight;
        var w = properties.context.canvas.width;
        var h = properties.context.canvas.height;

        if (w == clientW && h == clientH && !properties.isDataUpdated) return;
        properties.isDataUpdated = false;

        if (w != clientW || h != clientH) {
            properties.context.canvas.setAttribute('width', clientW);
            properties.context.canvas.setAttribute('height', clientH);
            properties.height = clientH;
            properties.width = clientW;
            properties.geometry.height = clientH;
            properties.geometry.width = clientW;
        }

        var offsetX = this.properties.offsetX;
        var offsetY = this.properties.offsetY;
        let _w = properties.geometry.width;
        let _h = properties.geometry.height;
        var height = _h - offsetY * 2;
        var width = _w - offsetX;
        var _x = properties.x;
        var _y = properties.y;
        var x = properties.x + offsetX;
        var y = properties.y + offsetY;
        properties.height = height;
        properties.width = width;

        var ctx = this.properties.context;

        var colorBG = properties.colors.bg;
        var colorAxes = properties.colors.axes;
        var colorLabel = properties.colors.label;
        var colorHoriz = properties.colors.horiz;

        let labels = this.properties.label;
        var centerX = 0.5 * _w + _x; //
        var centerY = 0.5 * _h + _y; //

        //clear canvas props
        ctx.fillStyle = '';
        ctx.strokeStyle = '';

        //Draw font         
        const boundingBox = new Path2D();
        boundingBox.rect(_x, _y, _w, _h);
        ctx.fillStyle = colorBG;
        ctx.fill(boundingBox);
        this.properties.boundingBox = boundingBox;

        //Draw Y axes
        for (let i in this.properties.axesY) {
            ctx.beginPath();
            let axY = this.properties.axesY[i];
            let dy = y + height - axY.pos * height;
            dy = (dy % 10 < 5 ? Math.round(dy) + 0.5 : Math.round(dy) - 0.5);
            ctx.moveTo(x, dy);
            //console.log(dy);

            if (!isNaN(axY.value)) {
                ctx.font = "12px serif";
                ctx.fillStyle = colorLabel;
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                ctx.fillText(axY.value, x - offsetX / 2, dy);
            }
            ctx.lineWidth = 1;
            ctx.strokeStyle = colorHoriz;
            ctx.lineTo(x + width, dy);
            ctx.stroke();
        }

        //console.log(this.properties.channels);

        for (let index in this.properties.data) {
            //Draw points
            ctx.beginPath();
            for (let i in this.properties.data[index].data) {
                let point = this.properties.data[index].data[i];

                let dx = x + (width / (this.properties.dataLength - 1)) * (parseInt(i) + this.properties.dataLength - this.properties.data[index].data.length); //отступ по X
                let py = (point.y - this.properties.valOffset) / properties.deltaVisual;
                let dy = y + height - (py * height);
                //console.log(x, width);
                //console.log(dy, dx, i , this.properties.dataLength , this.properties.data.length);

                if (i) {
                    ctx.strokeStyle = this.getColor(index);//colorLines
                    ctx.lineWidth = 2;
                    ctx.lineTo(dx, dy);
                    ctx.stroke();
                }
                ctx.beginPath();
                ctx.arc(dx, dy, 1, 0, Math.PI * 2, true);
                ctx.moveTo(dx, dy);
                ctx.fill();

                if (this.showPointValue()) {
                    ctx.font = "12px serif";
                    ctx.fillStyle = colorLabel;
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    if (typeof point.y == 'string') {
                        ctx.fillText(point.y, dx, dy - 10);
                    } else if (typeof point.y == 'number') {
                        ctx.fillText(point.y.toFixed(2).toString(), dx, dy - 10);
                    }
                }

                if (index == this.properties.data.length - 1) {

                    //Draw X axes
                    if (this.showAxesXValue() && (i < this.properties.data[index].data.length - 1)) {
                        let yX = y + height + 8;
                        ctx.fillStyle = colorLabel;
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        ctx.fillText(point.x, dx, yX);
                    }
                }
            }

            //Draw the label
            if (this.properties.data.length != 0) {
                let maxLabelWidth = 0;
                let fontSize = 14;
                let margin = 5;
                ctx.font = `${fontSize}px sans-serif`;
                ctx.textAlign = 'left';
                ctx.textBaseline = 'top';

                for (let i in this.properties.data) {
                    let label = this.properties.data[i].label;

                    let lw = ctx.measureText(label).width;
                    if (lw > maxLabelWidth) maxLabelWidth = lw;
                }

                let labelWidth = (fontSize + maxLabelWidth + margin) * this.properties.data.length;
                let labelX = centerX - labelWidth / 2;

                for (let i in this.properties.data) {
                    let label = "Ch: " + this.properties.data[i].label.index;
                    let marginI = i * (maxLabelWidth + fontSize + margin);

                    ctx.beginPath();
                    ctx.fillStyle = this.getColor(i);
                    ctx.rect(labelX + marginI, _y + 5, fontSize, fontSize);
                    ctx.fill();

                    ctx.fillStyle = colorLabel;
                    ctx.fillText(label, fontSize + labelX + marginI, _y + 5);
                    ctx.closePath();

                }
            }
        }

        //Draw axes
        ctx.beginPath();
        ctx.strokeStyle = colorAxes;
        ctx.moveTo(x, y);
        ctx.lineTo(x, y + height);
        ctx.lineTo(x + width, y + height);
        ctx.stroke();
    }

    this.updateProps = function () {
        this.properties.maxVal = undefined; //определение макс и мин значений массива
        this.properties.minVal = undefined;
        for (let index in this.properties.data) {
            for (let i in this.properties.data[index].data) {
                let point = this.properties.data[index].data[i];
                if (this.properties.maxVal == undefined) {
                    this.properties.maxVal = point.y;
                } else {
                    this.properties.maxVal = point.y > this.properties.maxVal ? point.y : this.properties.maxVal;
                }
                if (this.properties.minVal == undefined) {
                    this.properties.minVal = point.y;
                } else {
                    this.properties.minVal = point.y < this.properties.minVal ? point.y : this.properties.minVal;
                }


            }
        }

        //console.log(this.properties.data, this.properties.maxVal, this.properties.minVal);

        let delta = (this.properties.maxVal - this.properties.minVal) * 1.1; // разница между макс и мин значениями
        let axesNumber = Math.ceil(this.properties.height / this.properties.axisH) + 1; //кол-во горизонтальных линий на графике
        let k = Math.pow(10, (roundUp(Math.log10(axesNumber)) - (delta != 0 ? roundUp(Math.log10(delta)) : 0) + 2)); //коэффициент масштабирования
        this.properties.deltaVisual = (delta * k + (axesNumber - 1) - delta * k % (axesNumber - 1)) / k; //отображаемый диапазон от мин до макс
        let valPerAxes = this.properties.deltaVisual / (axesNumber - 1); //цена деления
        let offset = roundUp(this.properties.minVal / valPerAxes) * valPerAxes;  //смещение осей относительно 0 axesNumber * (Math.trunc(this.properties.minVal / axesNumber) + ((this.properties.minVal % axesNumber != 0) ? Math.sign(this.properties.minVal)*1 : 0))
        this.properties.valOffset = offset;

        //console.log(delta, axesNumber, k, this.properties.deltaVisual, valPerAxes, offset);

        this.properties.axesY = [];
        for (let i = 0; i < axesNumber; i++) {
            let axesY = this.properties.axesY;
            //console.log(i * valPerAxes + offset);

            axesY.push({
                value: (i * valPerAxes + offset).toFixed(3),
                pos: i / (axesNumber - 1)
            });
        }
    }

    this.pushValue = function (points) {
        for (let i in points) {
            let value = points[i].value;
            let label = points[i].label;
            let time = points[i].time;

            let data = this.properties.data.find(item => { return item.label == label; });
            // console.log(i, value, this.properties.data);

            if (data == undefined) this.properties.data.push({ label: label, data: [] });

            data = this.properties.data.find(item => { return item.label == label; }).data;

            if (data.length >= this.properties.dataLength) data.shift();

            let t = this.getTime(time);
            data.push({ x: t, y: value });
        }
        this.properties.isDataUpdated = true;
        this.updateProps();
    }
    this.updateData = function () {
        let data = this.properties.data;
        let channels = this.properties.channels;

        for (let i in data) {
            let row = data[i];
            let index = row.label.index;

            if (channels.includes(index)) continue;
            
            Vue.delete(row);
            data.splice(i, 1);
            
        }
        this.updateProps();
    }

    this.setLabel = function (val) {
        this.properties.label = val;
    }
    this.showPointValue = function () {
        if (isNull(this.properties.showValue)) return (this.properties.width / this.properties.dataLength) > 30;

        return this.properties.showValue;
    }
    this.showAxesXValue = function () {
        if (isNull(this.properties.showValue)) return (this.properties.width / this.properties.dataLength) > 30;

        return this.properties.showValue;
    }
    this.getTime = function (t) {
        let date = t ? (new Date(+t / 1000)) : (new Date());

        //var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

        /* let day = date.getUTCDate();
        let month = date.getMonth() + 1;
        let year = date.getFullYear();
     
        let h = date.getHours();
        let m = date.getMinutes(); */
        let s = date.getSeconds();
        let ms = date.getMilliseconds();

        return `${s}.${ms}`;//${day}-${month}-${year}  ${h}:${m}:
    }
    this.getZeroPosition = function () {
        let x = this.properties.geometry.width / 2;
        let y = this.properties.geometry.height / 2;
        return { 'x': x, 'y': y };
    }
    this.getColor = function (i) {
        return `hsl(${Math.floor(360 * i / this.properties.data.length)},100%,50%)`
    }
    this.isHited = function (x, y) {
        //console.log(x, y, this.properties.context.isPointInPath(this.properties.boundingBox, x, y));

        return this.properties.context.isPointInPath(this.properties.boundingBox, x, y);
    }

    this.includeChannel = function (channel) {
        console.log(this.properties.channels);

        return this.properties.channels.includes(channel);
    }
}

function roundUp(val) {
    return val < 0 ? Math.floor(val) : Math.ceil(val);
}