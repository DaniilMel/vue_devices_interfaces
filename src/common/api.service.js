import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";

const ApiService = {
  init(r) {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = r;
    Vue.axios.defaults.timeout = 1000;
  },

  get(resource, slug = "") {
    return Vue.axios.get(`${resource}`).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  },
};

export default ApiService;

export const DevicesService = {
  getDevices() {
    return Vue.axios.get(`command//GET_DEVICES?timeout=1000`);
  },

  getParams(data) {
    let str = JSON.stringify(data.params);
    return Vue.axios.get(`command/${data.device}/GETPARAMS?params=${str}&timeout=3000`);
  },

  setCommand(data) {
    let device = data.device;
    let command = data.command;
    let params = JSON.stringify(data.params);

    if(params == undefined) return Vue.axios.get(`command/${device}/${command}?timeout=500`);

    return Vue.axios.get(`command/${device}/${command}?params=${params}&timeout=500`);
  }
}

