var devCounter = 0;

export function Device(json) {
    this.id = devCounter++;
    this.name = json.devicename;
    this.deviceName = json.nameexec;
    this.status = json.status;
    [this.statusColor, this.colorName] = getStatusColorAndName(this.status);
    this.description = json.desc;

    this.getColorByIndex = function (i) {
        return getStatusColorAndName(i);
    }

    this.updateParams = function( newDevice ) {
        this.status = newDevice.status;
        [this.statusColor, this.colorName] = getStatusColorAndName(this.status);
    }
}

const statuses = [
    {
        index: 0,
        color: "#909399",
        name: "Неизвестен"
    },
    {
        index: 1,
        color: "#67C23A",
        name: "OK"
    },
    {
        index: 2,
        color: "#F56C6C",
        name: "Ошибка самотестирования"
    },
    {
        index: 3,
        color: "#F56C6C",
        name: "Ошибка инициализации"
    },
    {
        index: 4,
        color: "#E6A23C",
        name: "Нет соединения"
    },
    {
        index: 5,
        color: "#909399",
        name: "Отключено"
    },
    {
        index: 6,
        color: "#FF0000",
        name: "Ошибка"
    },
]

function getStatusColorAndName(index) {
    let item = statuses.find(item => { return item.index == index; });
    if (!item) return [statuses[0].color, statuses[0].name];

    return [item.color, item.name];
}