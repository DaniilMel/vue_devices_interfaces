import { mapGetters } from "vuex";

export const DeviceMixin = {
    data() {
        return {
            focused: '',
            answer: "",
            command: "",
            message: "",
            isReqLoaded: false,

            getParamsTimer: undefined,
        }
    },
    mounted: function() {
        this.setupChannels();
        if (this.deviceObj) {
            this.getParams();
        }
    },
    computed: {
        ...mapGetters(['readOnly'])
    },
    methods: {
        getParams() {
            for (let i = 1; i <= this.channelsCount; i++) {

                this.$store
                    .dispatch("getParams", {
                        device: this.deviceObj.name,
                        params: { channel: i }
                    })
                    .then(res => {
                        let params = JSON.parse(res.data.response);
                        if (!params) return;
                        this.setChannelsParams(params);
                    });
            }

            /* this.getParamsTimer = setTimeout(() => {
                this.getParams();
            }, 1000);
            this.$once('hook:beforeDestroy', () => { clearTimeout(this.getParamsTimer) } ); */
        },
        focus(prop) {
            this.focused = prop;
        },
        blur() {
            this.focused = '';
        },
        getDeviceId() {
            return `${this.device.name}-${this.device.id}`;
        },
        setupChannels() {
            for (let i = 0; i < this.channelsCount; i++) {
                this.channels.push(Object.assign({}, this.channelParams));
                this.channels[i].index = i;
            }
        },
        sendCommand(command, params) {
            let data = {
                device: this.deviceObj.name,
                command: command,
                params: params
            };
            this.answer = "Ожидание ответа...";
            var str = "";
            this.isReqLoaded = false;
            return this.$store
                .dispatch("sendCommand", data)
                .then(res => {
                    str = res.data.response;
                    return str;
                })
                .catch(e => {
                    str = e;
                })
                .finally(() => {
                    this.command = command;
                    this.message = str;
                    this.isReqLoaded = true;
                    str = command + ": " + str;
                    this.setAnswer(JSON.stringify(str));
                });
        },
        setAnswer(str) {
            this.answer = str;
            setTimeout(() => {
                this.answer = "";
            }, 5000);
        }
    },
}