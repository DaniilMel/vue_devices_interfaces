import { DevicesService } from "../common/api.service";
import { Device } from "../components/devices/device_interface";

const state = {
    devices: [],
    filter: null,
    readOnly: undefined
}

const getters = {
    devices(state) {
        if (state.filter) return state.devices.filter(item => item.name.toLowerCase().includes(state.filter));
        return state.devices;
    },
    readOnly(state) {
        return state.readOnly;
    },
    filter(state) {
        return state.filter;
    }
}

const actions = {
    getDevices(context) {

        //пример получаемых данных
        var arr = {
            "devices": [
                { "desc": "описание 11", "devicename": "SRV_1_MPS_1", "nameexec": "SRV_1_MPS_1", "params": "", "status": 1 },
                { "desc": "", "devicename": "SRV_1_MT16_1", "nameexec": "SRV_1_MT16_1", "params": "51008", "saveTofile": true, "status": 0 },
                { "desc": "", "devicename": "SRV_1_MPS_2", "nameexec": "SRV_1_MPS_2", "params": "51008", "saveTofile": true, "status": 2 },
                { "desc": "", "devicename": "SRV_2_MPS_1", "nameexec": "SRV_2_MPS_1", "params": "51008", "saveTofile": true, "status": 5 },
                { "desc": "", "devicename": "SRV_2_IDR_1", "nameexec": "SRV_2_IDR_1", "params": "51008", "saveTofile": true, "status": 3 },
                { "desc": "", "devicename": "SRV_3_MT16_1", "nameexec": "SRV_3_MT16_1", "params": "51008", "saveTofile": true, "status": 4 }
            ]
        };
        context.commit("setDevices", arr.devices);
        return;

        return DevicesService.getDevices().then((res) => {
            let arr = JSON.parse(res.data.response).devices;
            context.commit("setDevices", arr);
            context.commit("setReadOnly", false); // set read status
        }).catch((e) => { });
    },
    getParams(context, data) {
        return DevicesService.getParams(data);
    },
    sendCommand(context, data) {
        return DevicesService.setCommand(data);
    }
}

const mutations = {
    setDevices(state, arr) {
        for (let i in state.devices) { //ищем и удаляем несуществующие девайсы
            let device = state.devices[i];

            let isRemovedDevice = (arr.find(item => {
                return item.devicename == device.name;
            }) === undefined);

            if (isRemovedDevice) state.devices.splice(i, 1);
        }

        for (let i in arr) { //добавляем или обновляем девайсы
            let json = arr[i];
            let newDevice = new Device(json);

            let device = state.devices.find(item => {
                return item.name === newDevice.name;
            })

            if (device) {
                device.updateParams(newDevice);
            } else {
                state.devices.push(newDevice);
            }
        }
    },
    setReadOnly(state, value) {
        state.readOnly = value;
    },
    setFilter(state, value) {
        state.filter = value;
    }
}

export default {
    state,
    getters,
    actions,
    mutations
};