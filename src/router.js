import Vue from "vue";
import Router from "vue-router";
import DevicesVisual from "./views/DevicesVisual";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: DevicesVisual
    }
  ]
});
